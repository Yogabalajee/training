import java.util.Scanner;
abstract class Shape{
    int dim1;
    int dim2;
    abstract double area();
}

class Rectangle extends Shape {
    Rectangle(int l,int b)
    {
        dim1=l;
        dim2=b;
    }
    double area() {
        double ar;
        ar=dim1*dim2;
        return ar;
    }
}
class Triangle extends Shape {
    Triangle(int b,int h)
    {
        dim1=b;
        dim2=h;
    }
    double area() {
        double ar;
        ar=0.5*dim1*dim2;
        return ar;
    }
}
public class UseShape {
    public static void main(String args[]) {
        int ch,dim1,dim2;
        Scanner sc=new Scanner(System.in);
        do{
            System.out.println("menu");
            System.out.println("1.rectangle");
            System.out.println("2.triangle");
            System.out.println("enter the choice");
            ch=sc.nextInt();
                switch(ch) {
                    case 1:
                        System.out.println("enter the lenght and breath");
                        //Dim1 - lenght & Dim2 -Breath
                        dim1=sc.nextInt();
                        dim2=sc.nextInt();
                        Rectangle re=new Rectangle(dim1,dim2);
                        System.out.println("area="+re.area());
                        break;
                    case 2:
                        System.out.println("enter the base and height");
                        //Dim1 - base & Dim2 -height
                        dim1=sc.nextInt();
                        dim2=sc.nextInt();
                        Triangle t=new Triangle(dim1,dim2);
                        System.out.println("area="+t.area());
                        break;
                    default:
                        System.out.println("enter the valid choice");
                }
        }while(ch<=2);
    }
}
