import java.util.Scanner;
import convert.Time;
import convert.Currency;

public class Distance {
    
    static class DistanceConverter {
        protected double kilometer;
        protected double meter;
        protected double mile;
        
        public DistanceConverter(double kilometer,double meter,double mile) {
            this.kilometer = kilometer;
            this.meter = meter;
            this.mile = mile;
        }
        public void printConverter() {
            System.out.println("The Kilometer is"+this.kilometer);
            System.out.println("The meter is"+this.meter);
            System.out.println("The mile is"+this.mile);
        }
        public void Convert() {
            System.out.println("Enter the kilometer to convert meter");
            Scanner sc = new Scanner(System.in);
            System.out.println("1:KM to meter,2:km to mile");
            int ch=sc.nextInt(); 
            System.out.println("Enter the km");
            kilometer=sc.nextInt();
            switch(ch) {
                case 1:
                    this.meter = kilometer*1000;
                    break;
                case 2:
                    this.mile = kilometer*0.621371;
                    break;
                default:
                    System.out.println("enter a vaild number");
                }
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int converterChoice = 1;
        do {
            System.out.println("1)Distance 2)Time 3)Currency");
            int n = sc.nextInt();
            switch(n) {
                case 1:
                    DistanceConverter distCont = new DistanceConverter(0.0d,0.0d,0.0d);
                    distCont.Convert();
                    distCont.printConverter();
                    break;
                case 2:
                    System.out.println("time");
                    Time timesc = new Time(0,0,0);
                    timesc.TConvert();
                    timesc.TprintConverter();
                    break;
                case 3:
                    System.out.println("Currency");
                    Currency currencysc = new Currency(0.0f,0.0f,0.0f);
                    currencysc.CConvert();
                    currencysc.CprintConverter();
                    break;
                default:
                    System.out.println("Enter the given option");
                    continue;
            }
            System.out.println("press 1 to contiue to choose the converter else 0 to exit");
            converterChoice = sc.nextInt();
        }while(converterChoice == 1);
        
    }
}