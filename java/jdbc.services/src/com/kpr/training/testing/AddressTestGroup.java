package com.kpr.training.testing;

import java.sql.Connection;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.service.AddressService;
import com.kpr.training.service.ConnectionManager;
import com.kpr.training.service.ConnectionService;

public class AddressTestGroup {
	
	public Address address;
	public Address addressUpdate;
	
	long createResult;
	long updateResult; 
	Address addressGot;
	List<Address> addressList;
	long deleteResult;
	
	public Address addressRead;
	public Address addressRead1;
	
	public Address nullAddress;
	
	public AddressService addressService = new AddressService();
	
	public ConnectionManager connectionManager = new ConnectionManager();
	
	
	public void setUp() {
		
		address = new Address("35, Goundamani Nagar", "Madurai", 765876);
		
		addressUpdate = new Address("89, Santhanam Avenue", "Thanjavur", 987123);
		
		nullAddress = new Address("null", "null", 0);
		
	}
	
	@Test (priority = 1, description = "Create Address")
	public void createAddress() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				createResult = addressService.create(address);
				
				Assert.assertTrue(createResult > 0);
				
				if(createResult > 0) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	//@Test (priority = 2, description = "Update Address")
	public void updateAddress() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				addressService.update(addressUpdate);
				
				addressRead = addressService.read(addressUpdate.getId());
				
				Assert.assertEquals(addressRead.toString(), addressUpdate.toString());
				
				if(addressRead.toString() == addressUpdate.toString()) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
				
			}
		});
	}
	
	@Test (priority = 3, description = "Read Address")
	public void readAddress() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				addressGot = addressService.read(42);
				
				Assert.assertEquals(addressGot, address);
			
				if(addressGot.toString() == address.toString()) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	@Test (priority = 4, description = "Read All Address")
	public void readAllAddress() throws Exception{
		
		connectionManager.submit(() -> {
			try {
				addressList = addressService.readAll();
				
				Assert.assertTrue(addressList != null);
				
				if(addressList != null) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
		
	}
	
	//@Test (priority = 5, description = "Delete Address")
	public void deleteAddress() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				addressService.delete(25);
				
				addressRead1 = addressService.read(25);
				
				Assert.assertEquals(addressRead1.toString(), nullAddress.toString());
				
				if(addressRead1.toString() == nullAddress.toString()) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
	}
}
