package com.kpr.training.testing;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.service.ConnectionManager;
import com.kpr.training.service.ConnectionService;
import com.kpr.training.service.PersonService;
import com.kpr.training.validator.Validator;

public class PersonTestGroup {
	
	public Person person;
	public Address address;
	
	public Person updatePerson;
	public Address updateAddress;
	
	public Person readPerson;
	public Address readAddress;
	public Person personExpected;
	public Date birthDateExpected;
	public Date createdDateExpected;
	
	long createResult;
	long updateResult;
	long deleteResult;
	List<Person> personList;
	
	long personDelete;
	
	Person personRead;
	Person personRead1;
	
	PersonService personService = new PersonService();
	
	ConnectionManager connectionManager = new ConnectionManager();
	
	
	@BeforeMethod
	public void setUp() {
		
		person = new Person("Nedumaaran",
				"Rajangam",
				"sooraraipotru@gmcil.com",
				Validator.dateChanger("07-09-2001"));
		
		address = new Address("Chennai AirForce", "Chennai", 600001);
		
		updatePerson = new Person(59, "John", "Colin", "johncolin@gmail.com", Validator.dateChanger("18-07-2001"));
		
		personExpected = new Person(62,
				"Nedumaaran", 
				"Rajangam", 
				"sooraraipotru@gmail.com", 
				42, 
				Validator.dateChanger("21-06-1921"), 
				Validator.dateChanger("15-11-2020"));
		
		personDelete = 55;
	}
	
	@Test (priority = 1, description = "Person Creation")
	public void createPerson() throws Exception{
		
		connectionManager.submit(() -> {
			try {
				createResult = personService.create(person);
				
				Assert.assertTrue(createResult > 0);
				
				if(createResult > 0) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		});
	}
	
	@Test (priority = 2, description = "Person Update")
	public void updatePerson() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				personService.update(updatePerson);
				
				personRead = personService.read(updatePerson.getId(), false);
				
				Assert.assertEquals(personRead.toString(), updatePerson.toString());
				
				if(personRead.toString() == updatePerson.toString()) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch(Exception e) {
				e.printStackTrace();
			}
		});
		
	}
	
	@Test (priority = 3, description = "Person Read with Address")
	public void readPerson() throws Exception {
		
		
		connectionManager.submit(() -> {
			try {
				readPerson = personService.read(62, true);
				
				Assert.assertEquals(readPerson.toString(), personExpected.toString());
				
				if(readPerson.toString() == personExpected.toString()) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	@Test (priority = 4, description = "Read All Person")
	public void readAllPerson() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				personList = personService.readAll();
				
				Assert.assertTrue(personList != null);
				
				if(personList != null) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	@Test (priority = 5, description = "Delete a Person")
	public void deletePerson() throws Exception {
		
		connectionManager.submit(() -> {
			try {
				personService.delete(updatePerson.getId());
				
				personRead1 = personService.read(updatePerson.getId(), false);
				
				Assert.assertEquals(personRead1.toString(), updatePerson.toString());
				
				if(personRead1.toString() == updatePerson.toString()) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		
	}

}
