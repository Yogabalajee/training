package com.kpr.training.testing;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.kpr.training.service.AddressService;
import com.kpr.training.service.ConnectionManager;
import com.kpr.training.service.ConnectionService;
import com.kpr.training.exception.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.service.PersonService;
import com.kpr.training.validator.Validator;

import java.sql.Connection;

/**
 * Problem Statement 
 * 1.To Test All possible conditions for Service Classes
 * 
 * Requirement 
 * 1.To Test All possible conditions for Service Classes
 * 
 * Entity 
 * 1.ServicesTest 
 * 2.PersonService 
 * 3.AppException 
 * 4.Connection 
 * 5.Person
 * 6.Address
 * 
 * Method Signature 
 * 1.public void insertPersonWithAddress();
 * 2.public void insertPersonDuplicate(); 
 * 3.public void insertPersonNoAddress(); 
 * 4.public void insertPersonNoFields(); 
 * 5.public void readPersonWithAddress(); 
 * 6.public void readPersonWithoutAddress();
 * 7.public static void setUp();
 * 
 * Jobs to be Done 
 * 1.Create a method called connectionTest 
 * 2.Create a connection object called Connection type and call the getConnectio method of Connections
 * 		Class and store it. 
 * 3.Compare the result of the connection with Assert and assertEquals 
 * 4.Create a method called insertPersonWithAddress 
 * 5.Create Date of Date type 
 * 6.Create Person of person type and pass values 
 * 7.Create Address of address type and pass values 
 * 8.Create a personService object PersonService
 * 9.Call the insert method of personService class and pass the values, store the result in
 *    insertPersonStatus object of long type. 
 * 10.Check whether the insertPersonStatus object is greater than 0 with AssertTrue of Assert.
 * 11.Create a method called insertPersonDuplicate 
 * 12.Create date of Date type.
 * 13.Create person of Person type 
 * 14.Create address of Address type 
 * 15.Create PersonService object called personService 
 * 16.Call the insert method from the personService class and pass the person and address and store to
 * 		insertPersonStatus 
 * 17.Check the insertPersonStatus is greater than 0 with
 * assertTrue of Assert 18.Create method called insertPersonNoAddress 19.Create
 * a Date and person and pass the values for person 20.Create personService
 * object of PersonService class 21.Check the insertPersonStatus is greater than
 * 0 with assertTrue 22.Create method called insertPersonNoFields. 23.Create
 * personService object of PersonService 24.Create person and address type of
 * null values 25.call the insert method of personService class and pass the
 * person and address 25.Store the insert method values to the
 * insertPersonService 26.Check the insertPersonStatus is equal to 0 with
 * assertTrue of Assert 27.Create method called readPersonWithAddress 28.Create
 * person and address with null values 29.Create personService object of
 * PersonService 30.Call the readPerson method of personService and pass the
 * values to the parameter and store to the objectList of Object List type
 * 31.Check the objectList is not equal to null with assertTrue of Assert
 * 32.Create a readPersonWithoutAddress method 33.Create Person and Address
 * objects called person and address with no values 34.Create a personService
 * object of PersonService 35.Call the readPerson method of peronService and
 * pass the values to the method and store it to listObject of List<Oject>
 * 36.Check the listObject is not equal to null with assertTrue in Assert.
 * 37.Create a new Method called setUp and initialize all the variables created.
 * Pseudo Code
 * 
 * class ServicesTest {
 * 
 * long insertPersonStatus = 0;
 *
 * public static void setUp() {
 * 
 * ServicesTest servicesTest = new ServicesTest();
 * 
 * servicesTest.date = new Date(2001,98,18); servicesTest.person1 = new
 * Person("Dong Lee","dontlee7amArivu@gmail.com",servicesTest.date);
 * servicesTest.address1 = new Address("Ching mung Street","Ching
 * Ching",118624); servicesTest.date1 = new Date(1987,05,11);
 * servicesTest.person = new
 * Person("Kowsika","kowsika@gmail.com",servicesTest.date); servicesTest.address
 * = new Address("Tirupur","Tirupur",125331); servicesTest.date2 = new
 * Date(1987,12,12); servicesTest.person2 = new Person("Aadhi
 * vaasi","aadhivaasi@gmail.com",servicesTest.date2); servicesTest.date6 = new
 * Date(2001,98,18);
 * 
 * servicesTest.person6 = new Person("Dong
 * Lee","dontlee7amArivu@gmail.com",servicesTest.date6); servicesTest.address6 =
 * new Address("Ching mung Street","Ching Ching",118624);
 * 
 * servicesTest.date7 = new Date(2001,98,18); servicesTest.person7 = new
 * Person("Dong Lee","dontlee7amArivu@gmail.com",servicesTest.date7);
 * servicesTest.address7 = new Address("Ching mung Street","Ching
 * Ching",118624); }
 * 
 * @Test(priority = 1, description = "Connection Establishment",
 *                expectedExceptions = { AppException.class }) public void
 *                connectionTest() throws AppException { Connection connection =
 *                (Connection) Connections.getConnection();
 * 
 *                Assert.assertEquals(connection, connection); }
 * 
 * @Test(priority = 2, description = "Person Insertion with Address",
 *                expectedExceptions = { AppException.class }) public void
 *                insertPersonWithAddress() throws AppException { Date date =
 *                new Date(1987,05,11); Person person = new
 *                Person("Kowsika","kowsika@gmail.com",date); Address address =
 *                new Address("Tirupur","Tirupur",87908); PersonService
 *                personService = new PersonService(); insertPersonStatus =
 *                personService.insert(person, address);
 * 
 *                Assert.assertTrue(insertPersonStatus > 0); }
 * 
 * @Test(priority = 3, description = "Person Insertion Duplicate",
 *                expectedExceptions = { AppException.class }) public void
 *                insertPersonDuplicate() throws AppException { Date date = new
 *                Date(1987,05,11); Person person = new
 *                Person("Kowsika","kowsika@gmail.com",date); Address address =
 *                new Address("Tirupur","Tirupur",125331); PersonService
 *                personService = new PersonService(); insertPersonStatus =
 *                personService.insert(person, address);
 * 
 *                Assert.assertTrue(insertPersonStatus > 0); }
 * 
 * @Test(priority = 4, description = "Person Insertion without Address",
 *                expectedExceptions = { AppException.class }) public void
 *                insertPersonNoAddress() throws AppException{ Date date = new
 *                Date(1987,12,12); Person person = new Person("Aadhi
 *                vaasi","aadhivaasi@gmail.com",date);
 * 
 *                PersonService personService = new PersonService();
 *                insertPersonStatus = personService.insert(person,null);
 * 
 *                Assert.assertTrue(insertPersonStatus > 0); }
 * 
 * @Test(priority = 5, description = "Person Insertion without any fields",
 *                expectedExceptions = { AppException.class }) public void
 *                insertPersonNoFields() throws AppException { PersonService
 *                personService = new PersonService(); Person person = new
 *                Person(null, null, null); Address address = new Address(null,
 *                null, 0); insertPersonStatus = personService.insert(person,
 *                address);
 * 
 *                Assert.assertTrue(insertPersonStatus == 0); }
 * 
 * @Test(priority = 6, description = "Read Person with Address",
 *                expectedExceptions = { AppException.class }) public void
 *                readPersonWithAddress() throws AppException {
 * 
 *                Person person = new Person(null, null, null); Address address
 *                = new Address(null, null, insertPersonStatus);
 * 
 *                PersonService personService = new PersonService();
 *                List<Object> objectList = personService.readPerson(30, true);
 * 
 *                Assert.assertTrue(objectList != null); }
 * 
 * @Test(priority = 7, description = "Read Person without Address",
 *                expectedExceptions = { AppException.class }) public void
 *                readPersonWithoutAddress() throws AppException { Person person
 *                = new Person(null, null, null); Address address = new
 *                Address(null, null, insertPersonStatus);
 * 
 *                PersonService personService = new PersonService();
 * 
 *                List<Object> listObject = personService.readPerson(32, false);
 * 
 *                Assert.assertTrue(listObject != null); } }
 *
 */

public class ServicesTest {

	long person1Id;
	Person person;
	Address address1;
	
	Person personNull;
	Person personOneField;
	Person personTwoField;
	Person personThreeField;
	
	Date nullDate;
	Address addressNull;
	
	Person personRead;
	
	Person personUpdateOneField;
	Person personUpdateTwoField;
	Person personUpdateThreeField;

	public Person personSample1;

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");

	public static ServicesTest servicesTest = new ServicesTest();

	public static PersonService personService = new PersonService();
	
	ConnectionManager connectionManager = new ConnectionManager();

	@BeforeMethod
	public static void setUp() {

		servicesTest.person1Id = 58;

		servicesTest.person = new Person("Nedumaaran", "Rajangam", "sooraraipotru@gmail.com", Validator.dateChanger("18-04-1998"), servicesTest.address1);

		servicesTest.address1 = new Address("Tambaram Airforce", "Chennai", 982678);

		servicesTest.personNull = new Person(null, null, null, null, servicesTest.addressNull);

		servicesTest.addressNull = new Address(null, null, 0);
		
		servicesTest.nullDate = new Date(12,9,2000);
		
		servicesTest.personOneField = new Person("Ram", null, null, servicesTest.nullDate, null);
		
		servicesTest.personTwoField = new Person("John", "Wick", null, servicesTest.nullDate, null);
		
		servicesTest.personThreeField = new Person("John", "Wick", "johnwich@gmail.com", servicesTest.nullDate, null);
		
		servicesTest.personUpdateOneField = new Person("Sam", null, null, servicesTest.nullDate, null);
		
		servicesTest.personUpdateTwoField = new Person("Sam", "Sam", null, servicesTest.nullDate, null);
		
		servicesTest.personUpdateThreeField = new Person("Sam", "Sam", "sam@gmail.com", servicesTest.nullDate, null);

	}

	@Test(priority = 1, description = "Person Insertion with Address")
	public void insertPersonWithAddress() throws Exception {

		connectionManager.submit(() -> {
			try {
				long insertResult = personService.create(servicesTest.person);

				Assert.assertTrue(insertResult > 0);

				if (insertResult > 0) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Test(priority = 2, description = "Person Insertion Duplicate")
	public void insertPersonDuplicate() throws Exception {

		connectionManager.submit(() -> {
			try {
				long insertPersonStatus = personService.create(servicesTest.person);

				Assert.assertTrue(insertPersonStatus > 0);

				if (insertPersonStatus > 0) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Test(priority = 3, description = "Person Insertion without Address")
	public void insertPersonNoAddress() throws Exception {

		connectionManager.submit(() -> {
			try {
				long insertPersonStatus = personService.create(servicesTest.person);

				Assert.assertTrue(insertPersonStatus > 0);

				if (insertPersonStatus > 0) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Test(priority = 4, description = "Person Insertion without any fields")
	public void insertPersonNoFields() throws Exception {

		connectionManager.submit(() -> {
			try {
				long insertPersonStatus = personService.create(servicesTest.personNull);
				
				servicesTest.personRead = personService.read(insertPersonStatus, true);

				Assert.assertEquals(servicesTest.personRead, servicesTest.personNull);

				if (servicesTest.personRead == servicesTest.personNull) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	
	@Test(priority = 5, description = "Person Insertion with one field")
	public void insertPersonOneField() {
		
		connectionManager.submit(() -> {
			try {
				long insertPersonStatus = personService.create(servicesTest.personOneField);
				
				servicesTest.personRead = personService.read(insertPersonStatus, true);
				
				Assert.assertEquals(servicesTest.personRead, servicesTest.personOneField);
				
				if(servicesTest.personRead == servicesTest.personOneField) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	
	@Test(priority = 6, description = "Person Insertion with Two field")
	public void insertPersonTwoField() {
		
		connectionManager.submit(() -> {
			try {
				long insertPersonStatus = personService.create(servicesTest.personTwoField);
				
				servicesTest.personRead = personService.read(insertPersonStatus, true);
				
				Assert.assertEquals(servicesTest.personRead, servicesTest.personTwoField);
				
				if(servicesTest.personRead == servicesTest.personTwoField) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	@Test(priority = 7, description = "Person Insertion with Three field")
	public void insertPersonThreeField() {
		
		connectionManager.submit(() -> {
			try {
				long insertPersonStatus = personService.create(servicesTest.personThreeField);
				
				servicesTest.personRead = personService.read(insertPersonStatus, true);
				
				Assert.assertEquals(servicesTest.personRead, servicesTest.personThreeField);
				
				if(servicesTest.personRead == servicesTest.personThreeField) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	

	@Test(priority = 8, description = "Read Person with Address")
	public void readPersonWithAddress() throws Exception {

		connectionManager.submit(() -> {
			try {
				Person person = personService.read(servicesTest.person1Id, true);

				Assert.assertEquals(person.toString(), personSample1.toString());

				if (person == personSample1) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	@Test(priority = 9, description = "Read Person without Address")
	public void readPersonWithoutAddress() throws Exception {

		connectionManager.submit(() -> {
			try {
				
				Person person = personService.read(servicesTest.person1Id, false);

				Assert.assertEquals(person.toString(), personSample1.toString());

				if (person == personSample1) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	
	@Test(priority = 10, description = "Update Person with OneField")
	public void updatePeronWithOneField() {
		
		connectionManager.submit(() -> {
			try {
				personService.update(servicesTest.personUpdateOneField);
				
				personService.read(servicesTest.personUpdateOneField.getId(), true);
				
				Assert.assertEquals(servicesTest.personUpdateOneField, servicesTest.personUpdateOneField);
				
				if(servicesTest.personUpdateOneField == servicesTest.personUpdateOneField) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch(Exception e) {
				e.printStackTrace();
			}
		});
		
	}
	
	
	@Test(priority = 11, description = "Update Person with OneField")
	public void updatePeronWithTwoField() {
		
		connectionManager.submit(() -> {
			try {
				personService.update(servicesTest.personUpdateTwoField);
				
				personService.read(servicesTest.personUpdateTwoField.getId(), true);
				
				Assert.assertEquals(servicesTest.personUpdateTwoField, servicesTest.personUpdateTwoField);
				
				if(servicesTest.personUpdateTwoField == servicesTest.personUpdateTwoField) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch(Exception e) {
				e.printStackTrace();
			}
		});
		
	}
	
	
	@Test(priority = 12, description = "Update Person with OneField")
	public void updatePeronWithThreeField() {
		
		connectionManager.submit(() -> {
			try {
				personService.update(servicesTest.personUpdateThreeField);
				
				personService.read(servicesTest.personUpdateThreeField.getId(), true);
				
				Assert.assertEquals(servicesTest.personUpdateThreeField, servicesTest.personUpdateThreeField);
				
				if(servicesTest.personUpdateThreeField == servicesTest.personUpdateThreeField) {
					ConnectionService.commitRollback(true);
				} else {
					ConnectionService.commitRollback(false);
				}
				
				ConnectionService.release();
			} catch(Exception e) {
				e.printStackTrace();
			}
		});
		
	}

}
