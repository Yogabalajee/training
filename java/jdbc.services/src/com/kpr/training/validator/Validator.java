package com.kpr.training.validator;

import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.service.PersonService;


/**
 * Problem Statement
 * 1.Create a validator Class to validate the person and date
 * 
 * Entity
 * 1.Validator
 * 2.Person
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public static boolean personValidator(Person person, Connection connection);
 * 2.public static boolean dateValidator(String date);
 * 3.public static Date dateChanger(String date);
 * 
 * Jobs to be Done
 * 1.Create a method called personValidator with person and connection as parameter
 * 2.Create a long object called personId
 * 3.Create a preparedStatement called presonCheckerStatement and call the QueryStatement
 *   .PERSON_CHECKER query.
 * 4.Set the values to the statement
 * 5.Execute the query and store the value to the resultSet called result
 * 6.Check while the result has next get the long value of id and store to personId
 * 7.Check whether the personId is equal to 0
 * 		7.1)return false
 * 8.return true
 * 
 * 1.Create a method called dateValidator with String date as parameter
 * 2.Create variable called validateResult of boolean and Date obejct called dateOut
 * 3.Create a regex pattern and store to dateFormat
 * 4.Create a Pattern object and compile the dateFormat
 * 5.Create a Matcher object and match the date from the parameter
 * 6.if the matches returns false then throw new AppException(ErrorCodes.E421)
 * 7.Set validateResult to true
 * 8.return validateResult
 * 
 * 1.Create a method called dateChanger() with String as parameter
 * 2.Create a SimpleDateFormat object called dateFomatSimple with dd-mm-yyyy as format
 * 3.return the parsed date from string
 * 
 * Pseudo Code
 * public class Validator {
 *	
 *	public static boolean personValidator(Person person, Connection connection) {
 *		long personId = 0;
 *		
 *	 	try {
 *			PreparedStatement personCheckerStatement = 
 *					connection.prepareStatement(QueryStatement.PERSON_CHECKER);
 *			
 *			personCheckerStatement.setString(1, person.getFirstName());
 *			personCheckerStatement.setString(2, person.getLastName());
 * 		
 *			ResultSet result = personCheckerStatement.executeQuery();
 *			
 *			while(result.next()) {
 *				personId = result.getLong("id");
 *			}
 *			
 *		} catch (Exception e) {
 *			e.printStackTrace();
 *		}
 *		
 *		if(personId == 0) {
 *			return false;
 *		}
 *		return true;
 *	}
 *	
 *	public static boolean dateValidator(String date) throws Exception {
 *		
 *		boolean validateResult;
 *		Date dateOut;
 *		
 *		String dateFormat = "^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\\d\\d$";
 *		//"^(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/[0-9]{4}$"
 *		
 *		Pattern pattern = Pattern.compile(dateFormat);
 *		
 *		Matcher matcher = pattern.matcher(date);
 *		
 *		SimpleDateFormat dateFormatSimple = new SimpleDateFormat("dd-mm-yyyy");
 *		
 *		if(!matcher.matches()) {
 *			throw new AppException(ErrorCodes.E421);
 *		} 
 *		validateResult = true;
 *		return validateResult;
 *	}
 *	
 *	public static Date dateChanger(String date) throws Exception{
 *		Date dateOut;
 *		
 *		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
 *		
 *		return dateOut = dateFormat.parse(date);
 *		
 *	}
 *}
 * 
 * 
 *
 */
public class Validator {
	
	public static Date dateChanger(String date) {
		
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			
			LocalDate.parse(date, DateTimeFormatter.ofPattern("dd-MM-yyyy")
					.withResolverStyle(ResolverStyle.STRICT));
			
			return (Date) dateFormat.parse(date);
		} catch (Exception e ) {
			throw new AppException(ErrorCodes.E427, e);
		}
	}
	
	public static void main(String[] args) {
		
		dateChanger("18-06-2001");
		
	}
}
