package com.kpr.training.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.kpr.training.constants.Constants;

/**
 * Problem statement
 * 1.Create a ConnectionPool Class
 * 
 * Entity
 * 1.ConnectionPool
 * 2.ThreadLocal
 * 3.ConnectionService
 * 4.AppException
 * 5.ErrorCodes
 * 
 * Method Signature
 * 1.public Connection initialValue();
 * 2.public void remove(Connection connection);
 * 3.public void get(Connection connection);
 * 
 * Jobs to be Done
 * 1.Create a method called initalValue or override it with Connection as return type 
 * 2.return the ConnectionService.initConnection method
 * 3.Create a method called remove with connection object as parameter
 * 4.call the super.remove() method
 * 5.Call the releaseConnection method from ConnectionService and pass the connection 
 * 
 *  
 * Pseudo Code
 * class ConnectionPool extends ThreadLocal<Connection>{
 *	
 *	public Connection initialValue() {
 *		return ConnectionService.initConnection();
 *	} 
 *
 *	public void remove(Connection connection) {
 *		super.remove();
 *		try {
 *			ConnectionService.releaseConnection(connection);
 *		} catch (Exception e) {
 *			e.printStackTrace();
 *		}
 *	  }
 *	}
 *
 */

public class ConnectionManager extends ThreadPoolExecutor{
	
	private static ConnectionService connectionService;
	
	@Override
	protected void beforeExecute(Thread t, Runnable r) {
		connectionService = new ConnectionService();
		try {
			connectionService.initConnection();
			ConnectionService.get().setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		super.beforeExecute(t, r);
	}
	
	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		try {
			ConnectionService.get().close();
			if(ConnectionService.get().isClosed()) {
				ConnectionService.release();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		super.afterExecute(r, t);
	}

	public ConnectionManager() {
		super(Constants.MAX_CONNECTION, 
			  Constants.MAX_CONNECTION, 
			  0L, 
			  TimeUnit.MILLISECONDS,
			  new LinkedBlockingQueue<Runnable>());
	}
	
	
}

/**
 * Jobs to be Done
 * 1.Create a method called setUp 
 * 		1.1)Initialize the variable person1Id as 58
 * 		1.2)Initialize the variable date to 18
 * 
 */
