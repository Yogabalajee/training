package com.kpr.training.service;

import java.sql.Connection;

public class ConnectionPool extends ThreadLocal<Connection>{
	
	public Connection initialValue() {
		return ConnectionService.initConnection();
	}

	public void remove(Connection connection) {
		super.remove();
		try {
			ConnectionService.releaseConnection(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void get(Connection connection) {
		try {
			ConnectionService.getConnection(connection);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
