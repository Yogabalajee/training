package com.kpr.training.jdbc.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;

import com.kpr.training.constants.Constants;
import com.kpr.training.constants.QueryStatement;
import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodes;

/**
 * Problem Statement
 * 1.POJO Class for Address
 * 
 * Entity
 * 1.Address
 * 
 * Method Signature
 * 1.public Address(String street, String city, long postalCode);
 * 2.public Address(long id, String street, String city, long postalCode);
 * 3.public Address(long id);
 * 4.public long getId();
 * 5.public String getStreet();
 * 6.public String getCity();
 * 7.public long getPostalCode();
 * 8.public void setId(long id);
 * 9.public void setStreet(String street);
 * 10.public void setCity(String city);
 * 11.public void setPostalCode(long postalCode);
 * 12.public String toString();
 * 
 * Jobs to be Done
 * 1.Create a id field of long type
 * 2.Create a street field of String type
 * 3.Create a city field of String type
 * 4.Create a postal_code of int type
 * 5.Create a public constructor take all the Address fields
 * 6.Create the getter and setter for the Fields
 * 7.Override the toString method and concat with append and string builder
 * 
 * Pseudo Code
 * class Address {
 *
 *	private long id;
 *	private String street;
 *	private String city;
 *	private long postalCode;
 *	
 *	public Address(String street, String city, long postalCode) {
 *		this.street = street;
 *		this.city = city;
 *		this.postalCode = postalCode;
 *	}
 *	
 *	public Address(long id, String street, String city, long postalCode) {
 *		this.id = id;
 *		this.street = street;
 *		this.city = city;
 *		this.postalCode = postalCode;
 *	}
 *	
 *	public Address(long id) {
 *		this.id = id;
 *	}
 *	
 *	public long getId() {
 *		return id;
 *	}
 *	
 *	public String getStreet() {
 *		return street;
 *	}
 *	
 *	public String getCity() {
 *		return city;
 *	}
 *	
 *	public long getPostalCode() {
 *		return postalCode;
 *	}
 *	
 *	public void setId(long id) {
 *		this.id = id;
 *	}
 *	
 *	public void setStreet(String street) {
 *		this.street = street;
 *	}
 *	
 *	public void setCity(String city) {
 *		this.city = city;
 *	}
 *	
 *	public void setPostalCode(long postalCode) {
 *		this.postalCode = postalCode;
 *	}
 *	
 *	
 *	public String toString() {
 *		return new StringBuilder("Address [id=")
 *						.append(id)
 *						.append(", street=")
 *						.append(street)
 *						.append(", city =")
 *						.append(city)
 *						.append(", postalCode =")
 *						.append(postalCode)
 *						.append("]").toString();
 *	}
 */

public class Address {

	private long id;
	private String street;
	private String city;
	private long postalCode;
	
	public Address(String street, String city, long postalCode) {
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Address(long id, String street, String city, long postalCode) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postalCode = postalCode;
	}
	
	public Address(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public long getPostalCode() {
		return postalCode;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setPostalCode(long postalCode) {
		this.postalCode = postalCode;
	}
	
	
	public String toString() {
		return new StringBuilder("Address [id=")
						.append(id)
						.append(", street=")
						.append(street)
						.append(", city =")
						.append(city)
						.append(", postalCode =")
						.append(postalCode)
						.append("]").toString();
	}
	
}

/**
 * Problem Statement
 * 1.Create a method to values to preparedStatement from address
 * 
 * Entity
 * 1.Address
 * 2.AddressService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.private void statementValueSetterAddress(PreparedStatement ps, Address address);
 * 
 * Jobs to be Done
 * 1.Set the values to the preparedStatement from Address
 * 2.Catch the Exception
 * 
 * Pseudo Code
 * private void statementValueSetterAddress(PreparedStatement ps, Address address) {
 *		try {
 *			ps.setString(1, address.getStreet());
 *			ps.setString(2, address.getCity());
 *			ps.setLong(3, address.getPostalCode());
 *		} catch (Exception e) {
 *			throw new AppException(ErrorCodes.E428, e);
 *	 	}
 *	}
 */


