package com.kpr.training.connections;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Problem Statement
 * 1.Create a Connection Class
 * 
 * Entity
 * 1.Connections
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Create a Connection object called connection
 * 2.Pass the Constents url, Constents user, Constents pass to the getConnection method..
 * 3.Catch the SQL Exception
 * 4.return the connection.
 * 
 * Pseudo Code
 * 
 * class Connections {
 *	
 *	public static Connection connection ;
 *	
 *	public static Connection getConnection() {
 * 		try {
 *			connection = DriverManager.getConnection(Constents.url, Constents.user, Constents.pass);
 *		} catch (SQLException e) {
 *			e.printStackTrace();
 *		}
 *		return connection;
 *	}
 *}
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.testng.annotations.Test;

import com.kpr.training.constants.Constants;
import com.kpr.training.exception.AppException;

public class Connections {
	
	public static Connection connection ;
	
	public static Connection getConnection() throws AppException{
		
		Properties property = new Properties();
		/*
		try {
			InputStream fileInput = new FileInputStream("C:\\Users\\UKSD\\eclipse-workspace\\jdbc-service\\resource\\db.properties");
			
			property.load(fileInput);
			
			fileInput.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		*/
		
		try {
			/*connection = DriverManager
						.getConnection(property.getProperty("sqlUrl")
						,property.getProperty("sqlUser") 
						,property.getProperty("sqlPass"));
			*/
			
			connection = DriverManager.getConnection(Constants.URL,Constants.USER,Constants.PASS);
			if(connection != null) {
				System.out.println("Connection Established Successfully");
			} else {
				throw new AppException();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return connection;
	}
}