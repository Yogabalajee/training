/*
+ Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
	
Requirement:
	*To Demonstrate abstract classes using Shape class.	
	*To create methods for class to calculate area and perimeter
	*To define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
	
Entity
    1.Shape -Class name
    2.Circle - Class name 
    3.Rectangle - Class name
	
Jobs to be done Done
    1.First create Abstract Shape class with area Calculaing method as printArea
      and printPeriMeter method as periCalc for both circle and rectangle
    2.Then Create Circle class and rectangle class to implement  area and perimeter calculating methods
    3.Creating Instance for the classes , calling the area,perimeter and area methods.
    4.Then pass parameter to calculate.
*/


import java.io.*;
import java.util.*;
abstract class Shape {
    abstract public double printArea();
    abstract public double printPeriMeter();
}
class Square extends Shape {
    private double side;
    public Square(double side) {
        this.side= side;
    }
    public double printArea() {
        return side*side;
    }
    public double printPeriMeter() {
        return 4*side;
    }
}

class Circle extends Shape {
    private double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double printArea() {
        return (3.14f*(radius*radius));
    }
    public double printPeriMeter() {
        return 2*3.14f*radius;
    }
}

public class Jmain
{
    public static void main(String args[])
    {
        Scanner sc = new Scanner(System.in);   
        System.out.println("choose the Shape");
        System.out.println("1. Square 2.Circle");
        int choose = sc.nextInt();
        switch(choose)
        {
            case 1:
                System.out.println("Enter the length");
                int side = sc.nextInt();
                Square square =new Square(side);
                System.out.println("The Area of the Square is "+ square.printArea());
                System.out.println("The Perimeter of the Square is "+ square.printPeriMeter());
                break;
            case 2:
                System.out.println("Enter the radius");
                int radius = sc.nextInt(); 
                Circle circle =new Circle(radius);          
                System.out.println("The Area of the Circle is "+ circle.printArea());
                System.out.println("ThePerimeter of the Circle is "+ circle.printPeriMeter());
                break;
        }    
    }
}