/*+ What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }


Requirment:
    To debug the code.

Entities:
    class name - SomethingIsWrong

Function Declaration:none

Jobs To be Done:
    Since the public Class has a Rectangle Class incomplete object created
    1.Creating a Rectangle class 
    2.Declaring two variables as height and width and public method area to return area of rectangle
    3.Creating the Object for the Rectangle as myRect
    4.It throws as variable initializing Error.

Solution:
object will not be created for rectangle since object creation is incomplete. It will create a null value for myRect object. 
*/


class Rectangle {
    int width;
    int height;
    
    public int area() {
        return height*width;
    }
}

public class SomethingIsWrong {
    public static void main(String[] args) {
        Rectangle myRect = new Rectangle();
        myRect.width = 40;
        myRect.height = 50;
        System.out.println("myRect's area is " + myRect.area());
    }
}