/*
    Problem Statement
    1.What is the value of the following expression, and why?
    
    Entity
    1.ExpressVal Class
    
    Work Done
    1.Comparing the Values fo Integer and Long type of Wrapper Class
    2.Even the values of the Integer 2 look similar to Long 2,
      the Reference Value and the Memory Size of the Integer and Long is 
      far differ from both..
    3.Here Comparing the Integer and long with if Statement and checking the 
      value. It prints Equal if the Integer 2 is equals to Long 2, else it prints Not Equals.
*/
public class ExpressVal {
    public static void main(String[] args) {
        if(Integer.valueOf(2).equals(Long.valueOf(2))) {
            System.out.println("Values are Equal");
        } else {
            System.out.println("Values are Not Equal");
        }
        System.out.println(Integer.valueOf(2));
        System.out.println(Long.valueOf(2));
    }
}