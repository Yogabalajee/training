/*
   
Entity
    1.InterfaceDemo Class
    2.CharSequence Interface 
    
Jobs to be done:
*Import a class from the package java.lang.CharSequence.
*Then a method has been written to return the string backwards.

    

+ What methods would a class that implements the java.lang.CharSequence interface have to implement
		Created a Class and implemented CharSequence interface
      
+ What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }
	
	The interface methods are implemented. Interface should have unimplemented methods
	
+ Is the following interface valid?
    public interface Marker {}
		The Interface is Valid since the methods are not necessary.
		
		
+ Write a class that implements the CharSequence interface found in the java.lang package.
  Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.
	
*/

import java.lang.CharSequence;
public class InterfaceDemo implements CharSequence{
    public static String name;
    
    //Length Method Overridded//
    public int length() {
        return name.length();
    }
    //toString Method Overridded//
    public String toString() {
        return name;
    }
    //charAt Method Overridded//
    public char charAt(int index) {
        return name.charAt(index);
    }
    //subSequence Method Overridded//
    public CharSequence subSequence(int start,int end) {
        String str = name.substring(start,end);
        StringBuffer buffer = new StringBuffer(str);
        buffer.reverse();
        str = buffer.toString();
        return str;
    }
    
    public static void main(String[] args) {
        InterfaceDemo demo = new InterfaceDemo();
        demo.name = "Yoga Balajee ";
        System.out.println(demo.name.length()); // Length of String//
        System.out.println(demo.subSequence(0,name.length())); // Reverse of a String//
        System.out.println(demo.charAt(5)); // charAt in a String //
        System.out.println(demo.toString()); // to String Method //
        
    }
}