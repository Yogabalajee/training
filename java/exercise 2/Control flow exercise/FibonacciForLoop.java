/*+ print fibinocci using for loop

Requirement:
    To print fibonacci series using for loop.

Entity:
    class-Fibonacci

Function Declaration:
    Here there is no function is declared.

Jobs to be Done:
    1. Declare the class Fibonacci.
    2. First declare the range to which the Series to be printed and assign the first two numbers of
       the series under two different variables as a and b.
    3. Then use for loop. Assign the value to the new variable i. Continue the process till the 
       condition meets i <= n.
    4. Now add the values of a and b and store it in new variable sum.
    5. Now assign the value of b to a and the value of b to sum.
    6. Now print a.
*/

import java.util.Scanner;
public class FibonacciForLoop {
    public static void main(String[] args) {
        int n;
        int a = 0;
        int b = 1;
        int sum;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        for(int i = 1; i <= n; i++) {
            sum = a + b;
            a = b;
            b = sum;
            System.out.print(a + " ");
         }
    }
}