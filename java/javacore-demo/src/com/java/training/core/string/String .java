Consider the following code snippet.
if (aNumber >= 0)
    if (aNumber == 0)
        System.out.println("first string");
else 
    System.out.println("second string");
    System.out.println("third string");

1) What output do you think the code will produce if aNumber is 3?

Ans:
    second string
    third string

2) Write a test program containing the previous code snippet; make aNumber 3. What is the output of the program? Is it what you predicted? Explain why the output is what it is. In other words, what is the control flow for the code snippet?

Ans: 
    NestedIf

second string
third string
3 is greater than or equal to 0, so execution progresses to the second if statement. The second if statement's test fails because 3 is not equal to 0. Thus, the else clause executes (since it's attached to the second if statement). Thus, second string is displayed. The final println is completely outside of any if statement, so it always gets executed, and thus third string is always displayed.

Exercise: Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
Solution:

if (aNumber >= 0)
    if (aNumber == 0)
        System.out.println("first string");
    else
        System.out.println("second string");

System.out.println("third string");

Exercise: Use braces { and } to further clarify the code and reduce the possibility of errors by future maintainers of the code.
Solution:

if (aNumber >= 0) {
    if (aNumber == 0) {
        System.out.println("first string");
    } else {
        System.out.println("second string");
    }
}

System.out.println("third string")