+ Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";
	

Entities:
  No class is used in this program.

Function Declaration:
  No function is declared in this program.
	
By executing in Two Ways
      1.concat Function of String Class
      2.+ COncatenation Operator.
	
//Two Ways to Concatenate the Strings//
        String res = hi.concat(mom);//Using concat Function of String Class//
        System.out.println(res);
        String res1 = hi + mom; //Using + (String Concatenation) operator //
        System.out.println(res1);
    }