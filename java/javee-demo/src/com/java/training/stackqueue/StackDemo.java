package com.java.training.stackqueue;
/*  Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.

---------------------------------------------------------WBS---------------------------------------------------
1.Requirements :
  - Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
  Reverse List Using Stack with minimum 7 elements in list.
2.Entities:
  - StackDemo 
3.Function Declaration:
  - public static void main(String [] args)
4.Job to be done :  
   1.Create a class name StackDemo and declare main method
   2.In the main creating the Stack with generic String type and creating stream for displaying the Stack elements.
   3.Pushing 5 values to stack using push() method and using pop() method displaying top value in stack.
   4.Search the specified value using search() method and find size of stack using size() method.
   5.To print the index value in stack using specifying index in println.
   6.Printing the stack values using stream and for each.
   7.To reverse a stack creating another stack removing the values using remove() method and pushing to new created 
stack using push() method 
   8.Removing elements from stack adding to list using add() method.
 
 ----------------------------------------------------Program Code---------------------------------------------------
*/
import java.util.Stack;
import java.util.stream.Stream;
public class StackDemo {
    public static void main(String [] args) {
        Stack<String> colors = new Stack<String>();  //Cretaing Stack using generic type
        Stream<String> stream = colors.stream();
        colors.push("Blue");
        colors.push("Green");
        colors.push("Yellow");
        colors.push("White"); 
        colors.add("Red");        
        colors.pop();   //pop top element in stack
        System.out.println("--------------------Create a stack using generic type and implement-----------------");
        int index = colors.search("Blue");  //Initialise search variable
        int size = colors.size();        //Initialise size variable
        System.out.println(size);       //Printing the stack size 
        System.out.println(index);      //Printing the index of the element
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
        
        System.out.println("--------------------Reverse List Using Stack with minimum 7 elements in list-----------------");
        Stack<String> stack = new Stack<String>();
        while(colors.size() > 0) {
                  stack.push(colors.remove(0)); //Element is removed from list and push to stack
        }

        while(stack.size() > 0){
                colors.add(stack.pop()); //Element is pop from stack and add to list
        } 

        System.out.println(colors);
    }
}