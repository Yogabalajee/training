package com.java.training.nio;
/*
1) Perform file operations using exists(), createDirectory(), copy(), move() and delete() methods.
 --------------------------------WBS---------------------------------------

1.Requirements:
    - Program to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount().
    
2.Entities:
    - PathMethods
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Create the txt file using Files class createTempFile method.
          1.1)Using exist method print file exist or not. //true or false
    2.Create the txt file using Files class createTempFile method.
          2.1)Using copy method copy the content to another txt file.
          2.2)Print the file copy or not.
    3.Create the txt file using Files class createTempFile method.
          3.1)Resolve method to get the file 
          3.2)Using createDirectory method to create directory.
    4.Delete the file using Files class delete method.
          4.1)Print the file delete or not.
    5.Create two file path to move one to one another file.
          5.1)Try block using move method to move source to destination
          5.2)StandardCopyOption with REPLACE_EXISTING print move or not 
          5.3)Source file deleted moved after.
Pseudo Code:
''''''''''''
public class FileOpertaions {
	public static void main(String... args) throws IOException {
		//exist method
        Path path = Files.createTempFile("test-sample", ".txt");
        System.out.println("File to check: " + path);
        boolean exists = Files.exists(path);
        System.out.println("File to check exits: " + exists);
        
        //copy() method
        Path copy = Files.createTempFile("copy-sample", ".txt");
        System.out.println("file to copy: " + path);
        Path copiedFile = Files.copy(
        		copy,
                Paths.get("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\nio\" + copy.getFileName()),
                StandardCopyOption.REPLACE_EXISTING
        );
        System.out.println("file copied: " + copiedFile);
        
        //createDirectory() method
        Path tempPath = Files.createTempDirectory("test");
        Path dirToCreate = tempPath.resolve("test1");
        Path directory = Files.createDirectory(dirToCreate);
        System.out.println("directory created: " + directory);
        
        //delete
        Files.delete(path);
        boolean existsNow = Files.exists(path);
        System.out.println("File exits after deleting: " + existsNow);
        
        //Move file 
        Path sourcePath = Paths.get("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\nio\source.txt");
        Path destinationPath = Paths.get("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\nio\destination.txt");
        try {
            Files.move(sourcePath, destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Successfully Moved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

--------------------------------Program Code---------------------------------------*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileOpertaions {
	public static void main(String... args) throws IOException {
		//exist method
        Path path = Files.createTempFile("test-sample", ".txt");
        System.out.println("File to check: " + path);
        boolean exists = Files.exists(path);
        System.out.println("File to check exits: " + exists);
        
        //copy() method
        Path copy = Files.createTempFile("copy-sample", ".txt");
        System.out.println("file to copy: " + path);
        Path copiedFile = Files.copy(
        		copy,
                Paths.get("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\nio" + copy.getFileName()),
                StandardCopyOption.REPLACE_EXISTING
        );
        System.out.println("file copied: " + copiedFile);
        
        //createDirectory() method
        Path tempPath = Files.createTempDirectory("test");
        Path dirToCreate = tempPath.resolve("test1");
        Path directory = Files.createDirectory(dirToCreate);
        System.out.println("directory created: " + directory);
        System.out.println("dir created exits: " + Files.exists(directory));
        
        //delete
        Files.delete(path);
        boolean existsNow = Files.exists(path);
        System.out.println("File exits after deleting: " + existsNow);
        
        //Move file 
        Path sourcePath = Paths.get("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\nio\\source.txt");
        Path destinationPath = Paths.get("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\nio\\destination.txt");
        try {
            Files.move(sourcePath, destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Successfully Moved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
