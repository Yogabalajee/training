package com.java.training.nio.advance;
/*
14. Get the file names of all file with specific extension in a directory
--------------------------------WBS---------------------------------------------

1.Requirements:
    - Program to get the file names of all file with specific extension in a directory.
    
2.Entities:
    - SpecificFileExtentention
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Pass file path in File class contructor.
    2.Filter file using list method and FilenameFilter class store it in string array.
    3.Check the file name using toLowerCase method endsWith method return true or false.
    4.Print the specific file extention in the path. 
    
Pseudo Code:
''''''''''''
public class SpecificFileExtentention {
	public static void main(String a[]) {
		File file = new File(C:\Users\yogabalajee\Downloads\advance.nio\advance.nio\");
		String[] list = file.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String f : list) {
			System.out.println(f);
		}
	}
}
--------------------------------Program Code---------------------------------------
*/

import java.io.File;
import java.io.FilenameFilter;

public class SpecificFileExtentention {
	public static void main(String a[]) {
		File file = new File("C:\\Users\\yogabalajee\\Downloads\\advance.nio\\advance.nio\\");
		String[] ext = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String exts : ext) {
			System.out.println(exts);
		}
	}
}
