package com.java.training.nio.advance;
/*
 9. Given a path, check if path is file or directory
 --------------------------------WBS---------------------------------------------

1.Requirements:
    - Program to check if path is file or directory
    
2.Entities:
    - FileOrDiretory
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Get the file using File class passing parameter in File class.
    2.Invoke isFile method to check path is file or directory 
       2.1)Check and Print file or directory.
    
    
Pseudo Code:
''''''''''''
public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:/Users/santh/eclipse-workspace/JavaEE-Demo/advance.nio/sample nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
		
	}
}
--------------------------------Program Code---------------------------------------
*/

import java.io.File;

public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:\\Users\\yogabalajee\\Downloads\\advance.nio\\advance.nio\\sample nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
	}
}
