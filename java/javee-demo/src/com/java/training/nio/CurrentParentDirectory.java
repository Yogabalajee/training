package com.java.training.nio;
/*

1) Write a program to obtain current directory and parent directory 
 --------------------------------WBS---------------------------------------

1.Requirements:
    - Program to obtain current directory and parent directory
    
2.Entities:
    - RelativeAbsolutePath
    
3.Methodsignature:
   - public static void main(String[] args)
  
4.Jobs to be done:
    1.Get the absolut path using
      1.1)get method with toAbsolutePath and to get in string toString method.
    2.Create File class and pass parameter as path and file 
      2.1)Print parent using getParent method.
    
Pseudo Code:
''''''''''''
public class CurrentParentDirectory {
	public static void main(String[] args) {
		String path = Paths.get("").toAbsolutePath().toString();
		System.out.println("Working Directory = " + path);
		File file = new File(path, "CurrentParentDirectory.java");
		System.out.println("File: " + file);
		System.out.println("Parent: " + file.getParent());
	}
}

--------------------------------Program Code---------------------------------------*/

import java.io.File;
import java.nio.file.Paths;

public class CurrentParentDirectory {
	public static void main(String[] args) {
		String path = Paths.get("").toAbsolutePath().toString();
		System.out.println("Working Directory = " + path);
		File file = new File(path, "CurrentParentDirectory.java");
		System.out.println("File: " + file);
		System.out.println("Parent: " + file.getParent());
	}
}
