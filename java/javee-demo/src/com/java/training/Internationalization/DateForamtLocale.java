package com.java.training.Internationalization; 
/*
1. To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL. 
------------------------------WBS-----------------------------------------
 
1.Requirement:
     - Program to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
2.Entities:
     - NumberFraction
     
3.Function Declaration:
  	 - public static void main(String[] args)
  	 
4.Jobs To Be Done:
  	1.Create Date class to get current and print.
  	2.Create Locale class to get a time in specified laguage and country.
  	3.Using DateFormat class getTimeInstance method pass parameters 
  	      3.1)DEFAULT with locale object.
  	      3.2)MEDIUM with locale object.
  	      3.3)LONG with locale object.
  	      3.4)SHORT with locale object.
  	      3.5)FULL with locale object.
    4.Print the all Date format. 	      
    
PseudoCode:
'''''''''''

public class DateForamtLocale{
   public static void main(String args[]){
	   
       Date currentDate = new Date();  
       System.out.println("Current date is: "+currentDate); 
       
       Locale locale = new Locale("fr", "FR");
       String timeDefault = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.DEFAULT: "+timeDefault);   
 
       String dateMedium = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.MEDIUM: "+dateMedium);  
         
       String dateLong = DateFormat.getDateInstance(DateFormat.LONG, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.LONG: "+dateLong); 
      
       String dateShort = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.SHORT: "+dateShort); 

       String timeFull = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.FULL: "+timeFull);  
         
       
   }
}
------------------------------Program Codes-----------------------------------------
*/ 

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale; 
public class DateForamtLocale{
   public static void main(String args[]){
	   
	   //Current Date
       Date currentDate = new Date();  
       System.out.println("Current date is: "+currentDate); 
       
       //Default time
       Locale locale = new Locale("English", "IN");
       String timeDefault = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.DEFAULT: "+timeDefault);   
 
       //Medium date
       String dateMedium = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.MEDIUM: "+dateMedium);  
         
       //Long date
       String dateLong = DateFormat.getDateInstance(DateFormat.LONG, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.LONG: "+dateLong); 
       
       //Short date
       String dateShort = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.SHORT: "+dateShort); 
 
       //Full time
       String timeFull = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.FULL: "+timeFull);  
         
       
   }
}