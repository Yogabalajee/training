/*

REQUIREMENT:
TO complete the program using override annotation and show the output,

ENTITY:
Base
Derived

FUNCTION DECLARATION:
public void display() 
public void display(int x) 
public static void main(String args[])

Jobs to be done:
1. Import Annotation to use override annotation
2. create a parent class Base.
4. Create a Derived class extends Base.
5. using override annotation override,override the Derived class Display method
6. hence the method overriding fails and throws an error message "method does not override or implement
    a method from a supertype"
	
Pseudo Code:
class Base{
    public void display()
	{
    }
}
class Derived Extends Base{
    @Override
    public void display(int x)
	{
	public static void main(String args[]) 
    { 
        Derived obj = new Derived(); 
        obj.display(); 
    } 

}

*/
package com.java.training.Reflection.annotations;

class Base 
{ 
     public void display() 
     { 
         
     }

	public void display(int x) {
		
	}
     	 
} 
class Derived extends Base 
{ 
     @Override
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}
