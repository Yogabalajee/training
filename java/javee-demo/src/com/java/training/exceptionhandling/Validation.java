package com.java.training.exceptionhandling;
/*7.a)Why validation is important?
      When receiving input that needs to be validated before it can be used,validate all input before using it,to avoid leaving the application 
in a half valid state.
-------------------------------------Example--------------------------------------
       //Correct:
           check if user already exists
           validate user
           validate address

           insert user
           insert address  
Reason: All the data are validating before giving the input. 
     If the address turns out to be invalid you will still have inserted the user in the system,if suppose the user corrects it and try to 
register again,it will be told that the user already exists.

----------------------------------------------------------------------------------------  
b)Yes,the following code can be done as it is.
--------PROGRAM-------
              check if user already exists
              validate user
              validate address

              insert user
              insert address
------Reason---------
Here all the data are validating before giving the input.*/