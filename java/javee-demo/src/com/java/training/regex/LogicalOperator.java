package com.java.training.regex;

/* 1.)Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%. See the example output as shown below:
Enter unit price: 25
Enter quantity: 110
The revenue from sale: 2475.0$
After discount: 275.0$(10.0%)

 * REQUIRMENT:
 *      To write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
 *      
 * ENTITY REQUIRED:
 *     public class LogicalOperators
 *     
 *Jobs to be done:
 *      Create a class called calculateSale() and declare fields such as unit price,quantity,revenue and discount rate.
 *      get the quantity and unit price from the user.
 *      When the quantity is less than 100,calculate revenue=unitprice*quantity
 *      When the quantity lies between 100 and 200 calculate revenue=unitprice*quantity;discount_amount=revenue*discount_rate;revenue-=discount_amount;
 *      When the quantity is greater than 120 ,calculate discount_rate=(float)15/100;revenue=unitprice*quantity;discount_amount=revenue*discount_rate;revenue-=discount_amount;
*       Then print the revenue and dicount based on the input given by the user.
*       
* Pseudo Code:
* calculateSale(){

		float unitprice=0f;
		int quantity=0;
		float revenue=0f;
		float discount_rate=0f, discount_amount=0f;

		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Enter unit price:");
			unitprice=sc.nextFloat();
			System.out.print("Enter quantity:");
			quantity=sc.nextInt();
		}
* if(quantity<100)
			revenue=unitprice*quantity;
		else if(quantity>=100 && quantity<=120)
		{
			discount_rate=(float)10/100;
			revenue=unitprice*quantity;
			discount_amount=revenue*discount_rate;
			revenue-=discount_amount;
		}

		else if(quantity>120)
		{
			discount_rate=(float)15/100;
			revenue=unitprice*quantity;
			discount_amount=revenue*discount_rate;
			revenue-=discount_amount;
		}

		System.out.println("")
* 
* 
 */

import java.util.*;
public class LogicalOperator 
{
	public static void main(String[] args)
	{
		calculateSale();
	}

	static void calculateSale(){

		float unitprice=0f;
		int quantity=0;
		float revenue=0f;
		float discount_rate=0f, discount_amount=0f;

		try (Scanner sc = new Scanner(System.in)) {
			System.out.print("Enter unit price:");
			unitprice=sc.nextFloat();
			System.out.print("Enter quantity:");
			quantity=sc.nextInt();
		}
		if(quantity<100)
			revenue=unitprice*quantity;
		else if(quantity>=100 && quantity<=120)
		{
			discount_rate=(float)10/100;
			revenue=unitprice*quantity;
			discount_amount=revenue*discount_rate;
			revenue-=discount_amount;
		}

		else if(quantity>120)
		{
			discount_rate=(float)15/100;
			revenue=unitprice*quantity;
			discount_amount=revenue*discount_rate;
			revenue-=discount_amount;
		}

		System.out.println("The revenue from sale:"+revenue+"$");
		System.out.println("After discount:"+discount_amount+"$("+discount_rate*100+"%)");




	}

}