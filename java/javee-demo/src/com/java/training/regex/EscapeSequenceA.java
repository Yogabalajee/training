package com.java.training.regex;

/*REQUIRMENT:
 *  To write a jwrite a program to display  escape sequence in java
	 My favorite book is "Twilight" by Stephanie Meyer
   ENTITY:
      EscapeSequenceA
   JOBS TO BE DONE:
       Declare and name a class .
       Initialize the given string to a new variable and use the respective escape sequence to get in the given format.
       then print the string.
    PSEUDO CODE:
      public class EscapeSequenceA{
public static void main(String[] args) {
    String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer")
       
 
 */

public class EscapeSequenceA {
public static void main(String[] args) {
    String myFavoriteBook = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
    System.out.println(myFavoriteBook);
}
}