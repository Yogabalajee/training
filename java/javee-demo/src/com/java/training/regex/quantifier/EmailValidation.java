package com.java.training.regex.quantifier ;

/*
Requirement:
   1) To write a code for the validation of given mail id.

Entity:
    1)EmailValidation

Method :
    1)public static void main(String[] args)

Jobs to be done:
    1. Create a list of Strings that contains the list of mail id of the persons.
    2. Check for the valid mail id of the person.
        2.1) Based upon the result print the result in boolean value.
        
Pseudo code:
class EmailValidation {
    
    public static void main(String[] args) {
        List<String> emailId = new ArrayList<>();
        // add the emailId
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        
        for (String email : emailId) {
            Matcher matcher = pattern.matcher(email);
            // print true or false based on the emails.
       }
   }
}
 
*/

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
    
    public static void main(String[] args) {
        List<String> emailId = new ArrayList<>();
        emailId.add("yogabalajee@gmail.com");
        emailId.add("18cs154@kpriet.ac.in");
        emailId.add("kithi@gmail.com");
        emailId.add("saranabc@gmail.com");
        
        String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
        Pattern pattern = Pattern.compile(regex);
        
        for (String email : emailId) {
            Matcher matcher = pattern.matcher(email);
            System.out.println(email + " : " + matcher.matches());
        }
        
    }

}