package com.java.training.generics;
/*
1.Requirement:
    - Write a program to demonstrate generics - for loop, for list, set and map.
2.Entity:
    - GenericsCollection
3.Method signature:
    - public static void main(String[] args)
4.Jobs to be done:
   1.Create a list of elements that can store only specific type.
   2.Create a set of elements that can store only specific type.
   3.Create a map that can accept only specific types of pairs.
   4.Display all elements of specific type.
   
Psudeo Code:
''''''''''''
public class GenericsCollection {

    public static void main(String[] args) {
		List<String> students = Arrays.asList("Varun", "Barath", "Prabha", "Dharun", "Kala");
        Set<Integer> uniqueNumbers = new HashSet<>();
        ADD 5 ELEMENTS 
        Map<Integer, String> colleges = new HashMap<>();
        ADD 5 ELEMENTS KEY AND VALUE
        for (Map.Entry<?, ?> college : colleges.entrySet()) {
            System.out.println(college.getKey() +" - " +
                    colleges.get(college.getKey()));
        }

    }

}
-----------------------------------------Program Code------------------------------------------------
*/

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GenericsTypes {

    public static void main(String[] args) {
        @SuppressWarnings("unused")
		List<String> students = Arrays.asList("Varun", "Barath", "Prabha", "Dharun", "Kala");

        Set<Integer> uniqueNumbers = new HashSet<>();
        uniqueNumbers.add(10);
        uniqueNumbers.add(22);
        uniqueNumbers.add(21);
        uniqueNumbers.add(3);
        uniqueNumbers.add(22);

        Map<Integer, String> colleges = new HashMap<>();
        colleges.put(2702, "BAIT");
        colleges.put(2764, "KPRIET");
        colleges.put(2706, "MCET");
        colleges.put(2006, "PSG");
        colleges.put(2726, "KSRIET");

        for (Map.Entry<?, ?> college : colleges.entrySet()) {
            System.out.println(college.getKey() +" - " +
                    colleges.get(college.getKey()));
        }

    }

}