package com.java.training.generics;

/*5. What will be the output of the following program?
public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}
----------------------------------WBS--------------------------------------
 1.Requirements : 
     - What will be the output of the following program?
 2.Entities :
 	 - public class UseGenerics.
 3.Function Declaration :
 	 - public static void main(String[] args)
 4.Jobs To Be Done:
 		1.Create the count method which returns the count of odd numbers present in a list.
 		2.Create the main method and create a list reference.
 		3.Add elements inside a list.
 		4.Invoke a count method and printing the number of odd numbers.

5.Psudeo Code:
''''''''''''
	public class UseGenerics {
	    public static void main(String args[]){  
	        MyGen<Integer> m = new MyGen<Integer>();  
	        m.set("merit");
	        System.out.println(m.get());
	    }
	}
	class MyGen<T>
	{
	    T var;
	    void  set(T var)
	    {
	        this.var = var;
	    }
	    T get()
	    {
	        return var;
	    }
	}

 */

public class UseGenerics {
    public static void main(String args[]){  
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());
    }
}
class MyGen<T>
{
    T var;
    @SuppressWarnings("unchecked")
	void  set(String string)
    {
        this.var = (T) string;
    }
    T get()
    {
        return var;
    }
}
/*-----------Output------------
merit
 */ 
