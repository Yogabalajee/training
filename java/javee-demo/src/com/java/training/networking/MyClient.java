package com.java.training.networking;
/*
2.Build a client side program using ServerSocket class for Networking.
--------------------------------WBS---------------------------------------

1.Requirements:
    - Build a client side program using ServerSocket class for Networking.
2.Entities:
    - MyClient
3.Methodsignature:
    - public static void main(String[] args)
4.Jobs to be done:
    1.Try catch 
        2.1)Create a Socket class with localhost.
        2.2)Pass argument of DataOutputStream class with getOutputStream method.
        2.3)Write to server using writeUTF method
        2.4)Close and flush the dataOutput.
    2.Catch Exception.

Pseudo Code:
''''''''''''
public class MyClient {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("localhost", 6666);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

--------------------------------Program Code---------------------------------------*/


import java.io.DataOutputStream;
import java.net.Socket;

public class MyClient {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("localhost", 6666);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
