package com.java.training.networking;
/*
1.Build a server side program using Socket class for Networking.

--------------------------------WBS---------------------------------------

1.Requirements:
    - Build a client side program using ServerSocket class for Networking.
2.Entities:
    - MyClient
3.Methodsignature:
    - public static void main(String[] args)
4.Jobs to be done:
    1.Try catch 
        2.1)Create a ServerSocket class with localhost.
        2.2)Accept serverSocket using accpet mathod.
        2.3)Pass argument of DataOutputStream class with getInputStream method.
        2.4)Read to client passed string using readUTF method
        2.4)Print and close the dataOutput.
    2.Catch Exception.

Pseudo Code:
''''''''''''
public class MyServer {
	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(6666);
			Socket socket = serverSocket.accept();// establishes connection
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

--------------------------------Program Code---------------------------------------*/



import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(6666);
			Socket socket = serverSocket.accept();// establishes connection
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
