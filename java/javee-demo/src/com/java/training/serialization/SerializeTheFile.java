package com.java.training.serialization;
/*
Create a File, get the Following fields as input from the user 
    1.Name
    2.studentId
    3.Address
    4.Phone Number
now store the input in the File and serialize it, and again de serialize the File and print the 
content.
--------------------------------------WBS-----------------------------------------
1.Requirement:
   - Program to store the input in the File and serialize it, and again de serialize the File and print the 
content.

2.Entity:
   - SerializeTheFile 

3.Method Signature:
   - public static void main(String [] args)

4.Jobs to be done:
    1.Get an input from the user for name, stuentid, address, phonenumber.
    2.Try block
      2.1)Set the path to store ser file and store it in fileout.
      2.2)Create an object for OutputStream name out for fileOut.
      2.3)Write in using writeObject method and close ObjectOutputStream writeFile object.
    3.Catch IOException invoke printStackTrace method. 

Psudocode:
''''''''''
public class SerializeTheFile {
	   public static void main(String [] args) {
		      Scanner scanner = new Scanner(System.in);
		      Student student = new Student();
		      System.out.println("Name: ");
		      student.name = scanner.next();
		      System.out.println("Student Id: ");
		      student.studentId = scanner.next();
		      System.out.println("Address: ");
		      student.address = scanner.next();
		      System.out.println("Phone Number: ");
		      student.phonenumber = scanner.nextLong();
		      try {
		         FileOutputStream fileOut = new FileOutputStream("C:/Users/santh/eclipse-workspace/JavaEE-Demo/serialization/students.ser");
		         ObjectOutputStream writeFile = new ObjectOutputStream(fileOut);
		         writeFile.writeObject(student);
		         writeFile.close();
		         fileOut.close();
		         System.out.println("Serialized data is saved in student.ser");
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
	   }
}
--------------------------------------Program Code-----------------------------------------*/
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class SerializeTheFile {
	   public static void main(String [] args) {
		      Scanner scanner = new Scanner(System.in);
		      Student student = new Student();
		      System.out.println("Name: ");
		      student.name = scanner.next();
		      System.out.println("Student Id: ");
		      student.studentId = scanner.next();
		      System.out.println("Address: ");
		      student.address = scanner.next();
		      System.out.println("Phone Number: ");
		      student.phonenumber = scanner.nextLong();
		      try {
		         FileOutputStream fileOut = new FileOutputStream("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\serialization\\student.ser");
		         ObjectOutputStream writeFile = new ObjectOutputStream(fileOut);
		         writeFile.writeObject(student);
		         writeFile.close();
		         fileOut.close();
		         System.out.println("Serialized data is saved in student.ser");
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
	   }
}
