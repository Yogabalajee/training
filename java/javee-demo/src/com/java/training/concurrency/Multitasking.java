package com.java.training.concurrency;

/*Write a program of performing two tasks by two threads that implements Runnable interface.

REQUIRMENT:

	To write a program of performing two tasks by two threads that implements Runnable interface.

Entity:
	Multitasking

Pseudo Code:
class Multitasking
	 
	  Thread t1=new Thread()  
	  Thread t2=new Thread()  
	   
	     
	  t1.start();  
	  t2.start();  
	
*/
class Multitasking{  
	 public static void main(String args[]){  
	  Thread t1=new Thread(){  
	    public void run(){  
	      System.out.println("task one");  
	    }  
	  };  
	  Thread t2=new Thread(){  
	    public void run(){  
	      System.out.println("task two");  
	    }  
	  };  
	  
	  
	  t1.start();  
	  t2.start();  
	 }  
	}  