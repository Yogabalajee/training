+ Explain in words Concurrency and Parallelism( in two sentences).

CONCURRENCY:

Concurrency is when two or more tasks can start, run, and complete in overlapping time periods.
 It doesn't necessarily mean they'll ever both be running at the same instant.
 Concurrency is a approach that is used for decreasing the response time of the system by using the single processing unit
  For example, multitasking on a single-core machine.
  
PARALLELISM:

Parallelism condition that arises when at least two threads are executing simultaneously.
Parallelism is when tasks literally run at the same time, e.g., on a multicore processor.
It is used for increasing the throughput and computational speed of the system by using the multiple processors