package com.java.training.collections.streamsadvance;

import java.util.Iterator;
import java.util.List;

/**
 * Problem Statement
 * 1.Iterator
 *    - Iterate the roster list in Persons class and and print the person without using forLoop/Stream
 *    
 * Requirement
 * 1.Iterator
 *    - Iterate the roster list in Persons class and and print the person without using forLoop/Stream
 *    
 * Entity
 * 1.PersonIterator
 * 
 * Method Signature
 * 
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store in persons list
 * 2.Create a iterator for persons list called personIterator.
 * 3.While the personIterator has next person then
 * 		3.1) Print the next Person.
 * 
 * 
 * Pseudo Code
 * 
 * class PersonIterator {
 *	
 *	public static void main(String[] args ) {
 *		
 *		List<Person> persons = Person.createRoster();
 *		
 *		Iterator personIterator = persons.iterator();
 *		
 *		while(personIterator.hasNext()) {
 *			System.out.println(personIterator.next());
 *		}
 *		
 *	}
 *
 *}
 * 
 *
 */

public class PersonIterator {
	
	public static void main(String[] args ) {
		
		List<Person> persons = Person.createRoster();
		
		Iterator personIterator = persons.iterator();
		
		while(personIterator.hasNext()) {
			System.out.println(personIterator.next());
		}
		
	}

}
