package com.java.training.collections.streamsadvance;

import java.util.List;
import java.util.OptionalDouble;

/**
 * Problem Statement
 * 1.Reducer methods(ToInt, ToLong)
 *   - Write a program to find the average age of all the Person in the person List
 *   
 * Requirement
 * 1.Reducer methods(ToInt, ToLong)
 *   - Write a program to find the average age of all the Person in the person List
 *   
 * Entity
 * 1.ReducerMethods
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Invoke the Person Class createRoster method and store it in persons list.
 * 2.For each Person
 * 		2.1)Add each person's age as int by invoking the getAge method from Person Class
 * 		2.2)Calculate the average and save the average age in allAgeInt of int type
 * 3.For each Person
 * 		3.1)Add each person's age as int by invoking the getAge method from Person Class
 * 		3.2)Calculate the average and save the average age in allAgeLong of long type. 
 * 4.Print the allAgeInt and allAgeLong.
 * 
 * Pseudo Code
 * class ReducerMethods {
 *	
 *	public static void main(String[] args) {
 *		
 *		List<Person> persons = Person.createRoster();
 *		
 *		int ageAllInt = (int) persons.stream()
 *							   .mapToInt(Person::getAge)
 *							   .average()
 *							   .getAsDouble();
 *		
 *		System.out.println(ageAllInt);
 *		
 *		long ageAllLong = (long) persons.stream()
 *				   .mapToInt(Person::getAge)
 *				   .average()
 *				   .getAsDouble(); 
 *
 *		System.out.println(ageAllInt);
 *		
 *	} 
 *
 *}
 * 
 * 
 *
 */

public class ReducerMethods {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		int ageAllInt = (int) persons.stream()
									 .mapToInt(Person::getAge)
									 .average()
									 .getAsDouble();
		
		System.out.println(ageAllInt);
		
		long ageAllLong = (long) persons.stream()
									    .mapToInt(Person::getAge)
									    .average()
									    .getAsDouble();

		System.out.println(ageAllInt);
		
	}

}
