package com.java.training.collections.streamsadvance;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

/**
 * Problem Statement
 * 1.- Remove the only person who are in the newRoster from the roster list.
 *  - Remove the following person from the roster List:
 *       new Person(
 *           "Bob",
 *           IsoChronology.INSTANCE.date(2000, 9, 12),
 *           Person.Sex.MALE, "bob@example.com"));
 *           
 * Requirement
 *  1.- Remove the only person who are in the newRoster from the roster list.
 *  - Remove the following person from the roster List:
 *       new Person(
 *           "Bob",
 *           IsoChronology.INSTANCE.date(2000, 9, 12),
 *           Person.Sex.MALE, "bob@example.com"));
 *           
 * Entity
 * 1.RetainRemove
 * 
 * Method Signature
 * 
 * Jobs to be Done
 * 1.Invoke the createRoster method and store it in the persons list.
 * 2.Create a newRoster list and add the persons. 
 * 3.Add the newRoster persons to the persons list.
 * 4.Remove the persons in the newRoster list from the persons list.
 * 5.Check if the person list has a person named Bob
 * 		5.1) If true then remove the person from the list
 * 6.Print each person in the persons list.
 * 
 * Pseudo Code
 *		public class RetainRemove {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster() 
	persons.addAll(newRoster);
        
        persons.retainAll(newRoster);
        
        persons.stream()
        	   .filter(person -> persons.contains(person) ? persons.remove(person) : null);
        
        persons.forEach(person -> System.out.println(person));
		
	}

}
 *
 */

public class RetainRemove {
	
	public static void main(String[] args) {
		
		List<Person> persons = Person.createRoster();
		
		List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        
        persons.addAll(newRoster);
        
        persons.retainAll(newRoster);
        
        persons.stream()
        	   .filter(person -> persons.contains(person) ? persons.remove(person) : null);
        
        persons.forEach(person -> System.out.println(person));
		
	}

}
