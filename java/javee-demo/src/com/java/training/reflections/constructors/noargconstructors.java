package com.java.training.reflections.constructors;
/*
 Requirements:
   - To create a constructor of type noargconstructor.
 Entity:
   - Private Constructor
 Function Declaration:
   - public static void main(String[] args)
 Jobs to be done:
   1. Create an example for a No-Arg Constructor.
   2. Display the output.
*/

class Main {

   int i;
private static Main obj;

   // constructor with no parameter
   private Main(){
       i = 5;
       System.out.println("Object created and i = " + i);
   }

   public static void main(String[] args) {

       setObj(new Main());
   }

public static Main getObj() {
	return obj;
}

public static void setObj(Main obj) {
	Main.obj = obj;
}
}