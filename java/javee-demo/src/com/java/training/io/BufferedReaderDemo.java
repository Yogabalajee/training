/*
Requirement:
    To read any textfile using bufferedReader and print the content of the file.

Entity:
    BufferedReaderDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1. In try block,Create file input stream and give the path of the file
        1.1 In First,Set the path to read the file and declare the variable as i
        1.2 In while condition,reading the file until it gets false 
        1.3 Print the result
    2.After reading the file,Use close() method to close the file.

Pseudocode:
public class BufferedReaderDemo {

	public static void main(String[] args) {
          // create try and catch block
		 try {    
		    FileInputStream fin = new FileInputStream("E:\\reader.txt");    
		    BufferedInputStream bin = new BufferedInputStream(fin);  //set the path to read the file  
		    int i;    
		    while((i=bin.read())!=-1) {    
		     System.out.print((char)i);    
		    }    
		    bin.close();    
		    fin.close();    
		  } catch(Exception e)
		 {
			  System.out.println(e);
		 }      
	}

}

*/

package com.java.training.io;

import java.io.*; 

public class BufferedReaderDemo {

	public static void main(String[] args) {
		 try {    
		    FileInputStream fin=new FileInputStream("C:\\1java\\IoExample.txt");    
		    BufferedInputStream bin=new BufferedInputStream(fin);    
		    int i;    
		    while((i=bin.read())!=-1) {    
		     System.out.print((char)i);    
		    }    
		    bin.close();    
		    fin.close();    
		  }catch(Exception e)
		 {
			  System.out.println(e);
		 }      
	}

}