/*
Requirement:
    To Write some String content using outputstream

Entity:
    OutputStreamDemo

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1.An IO exception is thrown
    2.Set the path of the file and create object as fos
        2.1 Declare the String
        2.2 Get the string in byte format and write it to the file
    3.Use close() method,to close the file.
    4.Print the output

Pseudocode:
public class OutputStream1 {

	public static void main(String[] args) throws IOException {
		 OutputStream fos = new FileOutputStream("E:\\reader.txt");    //set the path of file
			String s = "A small key opens big doors";
			byte[] b = s.getBytes();//converting string into byte array    
			
	                fos.write(b);
			fos.close();
			System.out.println("Success");
	        }
}
*/
package com.java.training.io;

import java.io.*; 

public class OutputStreamDemo {

	public static void main(String[] args) throws IOException {
		 OutputStream fos = new FileOutputStream("C:\\1java\\IoExample.txt");//set the path of file
			String s = "A small key opens big doors";
			byte[] b = s.getBytes();//converting string into byte array    
			
	                fos.write(b);
			fos.close();
			System.out.println("Success");
	        }
	
}