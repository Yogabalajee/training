/*
Requirements:
    To read a file using java.io.file

Entities:
    ReadFile

Method Signature:
    public static void main(String[] args)

Jobs to be done:
    1.In main method,try and catch is used
    2.Set the path of the file and use scanner() to get the input
    3.In while loop,read the file and print it
    4.Use close() method,to close the readingfile.

Pseudocode:
public class ReadFile {
    public static void main(String[] args) {
        try {
        File myObj = new File("E:\\example.txt");//path of the file
        Scanner myReader = new Scanner(myObj);//scanner() method
        while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        System.out.println(data);
      }
        myReader.close();
    }   catch(Exception e) {
        System.out.println(e);
  }
    }
  }

 */
package com.java.training.io;

import java.io.File; 
import java.util.Scanner;

public class ReadFile {
    public static void main(String[] args) {
        try {
            File myObj = new File("C:\\1java\\IoExample.txt");
            Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            System.out.println(data);
      }
        myReader.close();
    }   catch(Exception e) {
            System.out.println(e);
  }
    }
  }