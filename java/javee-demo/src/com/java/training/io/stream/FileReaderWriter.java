package com.java.training.io.stream; 
/* 
3.Write a Java program reads data from a particular file using FileReader 
and writes it to another, using FileWriter.

----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program reads data from a particular file using FileReader and writes it to another, 
using FileWriter.

2.Entity:
    - FileReaderWriter

3.Method Declaration:
    - public static void main(String[] args)
    
4.Jobs to be done :
     1.Pass the file path in FileReader class as argument.
     2.Pass the another file path to write a file in FileWriter class as argument.
     3.Read file using check while reader read method is not equal to -1.
         3.1)Store the each character in charRead and write in a file using write method.
     
     
Pseudo Code:
''''''''''''
public class FileReaderWriter {
	public static void main(String[] args) throws IOException {
		String readerPath = "C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\Content";
		FileReader reader = new FileReader(readerPath);
		String writerPath = "C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\AnotherContent";
		FileWriter writer = new FileWriter(writerPath);
		
		int charRead = -1;
		while ((charRead = reader.read()) != -1) {
			writer.write((char)charRead);
		}
		System.err.println("File Successfully Copied");
		writer.close();
	}
}

---------------------------------Program Code--------------------------------*/

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriter {
	public static void main(String[] args) throws IOException {
		String readerPath = "C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\Content";
		@SuppressWarnings("resource")
		FileReader reader = new FileReader(readerPath);
		String writerPath = "C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\AnotherContent";
		FileWriter writer = new FileWriter(writerPath);
		
		int charRead = -1;
		while ((charRead = reader.read()) != -1) {
			writer.write((char)charRead);
		}
		System.err.println("File Successfully Copied");
		writer.close();
	}
}
