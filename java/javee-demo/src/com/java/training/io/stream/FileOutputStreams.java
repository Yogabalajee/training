package com.java.training.io.stream; 
/*
5.Java program to write and read a file using output streams.
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program to write and read a file using output streams.

2.Entity:
    - FileOutputStreams

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Try block Pass the file path in File class argument.
       1.1.Pass the file object in FileInputStream and FileOutputStream class.
       1.2.Check the readFile is not equal to -1 and store a character using read method.
       1.3.Write file readed content to another file.
     2.Catch FileNotFoundException and IOException errors.
     
Pseudo Code:
''''''''''''
public class FileOutputStreams {
    public static void main(String[] args) {
        try {
            File inputFile = new File("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\\Content");
            File outputFile = new File("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\\AnotherContent");

            FileInputStream readFile = new FileInputStream(inputFile);
            FileOutputStream writeFile = new FileOutputStream(outputFile);
            int character;

            while ((character = readFile.read()) != -1) {
               writeFile.write(character);
            }
            System.out.println("Successfully Copied");
            readFile.close();
            writeFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("FileStreamsTest: " + e);
        } catch (IOException e) {
            System.err.println("FileStreamsTest: " + e);
        }
    }
}
---------------------------------Program Code--------------------------------*/


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreams {
    public static void main(String[] args) {
        try {
            File inputFile = new File("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\Content");
            File outputFile = new File("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\AnotherContent");

            FileInputStream readFile = new FileInputStream(inputFile);
            FileOutputStream writeFile = new FileOutputStream(outputFile);
            String text = "This Content writtern by using Writer";
            int character;

            while ((character = readFile.read()) != -1) {
               writeFile.write(character);
            }
            System.out.println("Successfully Copied");
            readFile.close();
            writeFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("FileStreamsTest: " + e);
        } catch (IOException e) {
            System.err.println("FileStreamsTest: " + e);
        }
    }
}