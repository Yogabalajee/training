package com.java.training.io.stream; 
/*
2.write a program that allows to write data to multiple files together using bytearray outputstream 
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program to write data to multiple files together using bytearray outputstream.

2.Entity:
    - ByteArrayOutputStreamDemo

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Store FileOutputStream and ByteArrayOutputStream as null value.
     2.Try block pass the file path FileOutputStream class argument to create file.
           2.1)Store the content in string.
           2.2)Get the string as bytes using getBytes method.
           2.3)Write the bytes in the files.
     3.Using writeTo method write the content in th files.
     4.Flush the files using flush method
     
Pseudo Code:
''''''''''''
public class ByteArrayOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream1 = null;
        FileOutputStream fileOutputStream2 = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            fileOutputStream1 = new FileOutputStream("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\\file1.txt");
            fileOutputStream2 = new FileOutputStream("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\\file2.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();

            String string = "Sample File Created";
            byte[] byteArray = string.getBytes();
            byteArrayOutputStream.write(byteArray);

           
            byteArrayOutputStream.writeTo(fileOutputStream1);
            byteArrayOutputStream.writeTo(fileOutputStream2);

            byteArrayOutputStream.flush();
            System.out.println("Successfully written to two files...");
        }
    }
}
---------------------------------Program Code--------------------------------*/


import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteArrayOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream1 = null;
        FileOutputStream fileOutputStream2 = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            fileOutputStream1 = new FileOutputStream("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\file1.txt");
            fileOutputStream2 = new FileOutputStream("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\file2.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();

            String string = "Sample File Created";
            byte[] byteArray = string.getBytes();
            byteArrayOutputStream.write(byteArray);

            
            byteArrayOutputStream.writeTo(fileOutputStream1);
            byteArrayOutputStream.writeTo(fileOutputStream2);

            byteArrayOutputStream.flush();
            System.out.println("Successfully written to two files...");
        }
        finally {
            if (fileOutputStream1 != null)
            {
                fileOutputStream1.close();
            }
            if (fileOutputStream2 != null)
            {
                fileOutputStream2.close();
            }
            if (byteArrayOutputStream != null)
            {
                byteArrayOutputStream.close();
            }
        }
    }
}