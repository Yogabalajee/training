package com.java.training.io.stream; 
/*
4.write a program for ByteArrayInputStream class to read byte array as input stream.
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program for ByteArrayInputStream class to read byte array as input stream.

2.Entity:
    - ByteArrayAsInput

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Store in byteArray an array values of integer values.
     2.Pass the byteArray in ByteArrayInputStream class of argument 
     3.Read using ByteArrayInputStream class of object bufferRead
     4.Check bufferRead read not equal to -1 and store in number.
     
Pseudo Code:
''''''''''''
public class ByteArrayAsInput {  
  public static void main(String[] args) throws IOException {  
    byte[] byteArray = { 5, 1, 2, 3 };  
    ByteArrayInputStream bufferRead = new ByteArrayInputStream(byteArray);  
    int numbers;  
    while ((number = bufferRead.read()) != -1) {     
      System.out.println(number);
    }  
  }  
} 
---------------------------------Program Code--------------------------------*/


import java.io.ByteArrayInputStream;
import java.io.IOException;  
public class ByteArrayAsInput {  
  public static void main(String[] args) throws IOException {  
    byte[] byteArray = { 5, 1, 2, 3 };  
    // Create the new byte array input stream  
    ByteArrayInputStream bufferRead = new ByteArrayInputStream(byteArray);  
    int numbers;  
    while ((numbers = bufferRead.read()) != -1) {  
      //Conversion of a byte into character   
      System.out.println(numbers);
    }  
  }  
} 