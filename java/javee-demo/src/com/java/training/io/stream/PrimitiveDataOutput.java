package com.java.training.io.stream; 
/*
6.program to write primitive datatypes to file using by dataoutputstream.
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program to primitive datatypes to file using by dataoutputstream.

2.Entity:
    - BufferReadWrite

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Store int, String, float values
     2.Pass file path in FileOutputStream class argument
     3.Using DataOutputStream class write method of with type adding to the dat file
     4.Print the added content in separate key.
     
Pseudo Code:
''''''''''''
public class PrimitiveDataOutput {
  public static void main(String[] args) throws Exception{
    FileOutputStream fos = new FileOutputStream("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\cities.dat");
    DataOutputStream dos = new DataOutputStream(fos);
    dos.flush();
    dos.close();
    FileInputStream fis = new FileInputStream("cities.dat");
    DataInputStream dis = new DataInputStream(fis);
    int cityId = dis.readInt();
    System.out.println("City Id: " + cityId);
    String cityName = dis.readUTF();
    System.out.println("City Name: " + cityName);
    int cityPopulation = dis.readInt();
    System.out.println("City Population: " + cityPopulation);
    float cityTemperature = dis.readFloat();
    System.out.println("City Temperature: " + cityTemperature);
  }
}
---------------------------------Program Code--------------------------------*/

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class PrimitiveDataOutput {

  public static void main(String[] args) throws Exception{
    int idA = 1;
    String nameA = "City";
    int populationA = 5;
    float tempA = 1.0f;

    int idB = 2;
    String nameB = "S";
    int populationB = 2;
    float tempB = 1.45f;

    FileOutputStream fos = new FileOutputStream("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\cities.dat");
    DataOutputStream dos = new DataOutputStream(fos);

    dos.writeInt(idA);
    dos.writeUTF(nameA);
    dos.writeInt(populationA);
    dos.writeFloat(tempA);

    dos.writeInt(idB);
    dos.writeUTF(nameB);
    dos.writeInt(populationB);
    dos.writeFloat(tempB);

    dos.flush();
    dos.close();

    FileInputStream fis = new FileInputStream("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\cities.dat");
    DataInputStream dis = new DataInputStream(fis);

    int cityId = dis.readInt();
    System.out.println("City Id: " + cityId);
    String cityName = dis.readUTF();
    System.out.println("City Name: " + cityName);
    int cityPopulation = dis.readInt();
    System.out.println("City Population: " + cityPopulation);
    float cityTemperature = dis.readFloat();
    System.out.println("City Temperature: " + cityTemperature);
  }
}
