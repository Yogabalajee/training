package com.java.training.io.stream; 
/*
5.Java program to write and read a file using buffer input.
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program to write and read a file using buffer input.

2.Entity:
    - PrimitiveDataOutput

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Store file path in writerPath String.
          1.1)Create FileWriter with file path in argument of BufferedWriter class.
          1.2)Write some content in file and close the bufferWriter using close method.
     2.Pass file path in File argument in BufferedReader.
     3.Read the file till the file null using while loop. 
         3.1)Store reader String to the file null.
     
Pseudo Code:
''''''''''''
public class BufferReadWrite {
	public static void main(String[] args) throws Exception {
		String writerPath = "C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\\BufferFile.txt";
		BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(writerPath));
		bufferWriter.write("Welcome to BufferWriter file");
		bufferWriter.close();
		System.out.println("Successfully Writtern");
		File file = new File("C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\\BufferFile.txt");
		BufferedReader bufferReader = new BufferedReader(new FileReader(file));
		String reader;
		while ((reader = bufferReader.readLine()) != null)
			System.out.println(reader);
	}
}
---------------------------------Program Code--------------------------------*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class BufferReadWrite {
	public static void main(String[] args) throws Exception {
		String writerPath = "C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\BufferFile.txt";
		BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(writerPath));
		bufferWriter.write("Welcome to BufferWriter file");
		bufferWriter.close();
		System.out.println("Successfully Writtern");

		File file = new File("C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\BufferFile.txt");

		@SuppressWarnings("resource")
		BufferedReader bufferReader = new BufferedReader(new FileReader(file));

		String reader;
		while ((reader = bufferReader.readLine()) != null)
			System.out.println(reader);
	}
}
