package com.java.training.io.stream; 
/*
1.How to read the file contents  line by line in streams 
----------------------------------------WBS---------------------------------------
1.Requirement :
    - Program to read the file contents  line by line in streams.

2.Entity:
    - ReadFileStream

3.Method Declaration:
    - public static void main(String[] args)

4.Jobs to be done :
     1.Store file path in fileName String.
     2.Get the file path using Paths class get method and pass parameter to lines method.
     3.Print the file content using for each.
          
     
Pseudo Code:
''''''''''''

public class ReadFileStream {
	public static void main(String args[]) {
		String fileName = "C:\Users\yogabalajee\eclipse-workspace\javaee-demo\src\com\java\training\io\stream\Content";
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
---------------------------------Program Code--------------------------------*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileStream {

	public static void main(String args[]) {

		String fileName = "C:\\Users\\yogabalajee\\eclipse-workspace\\javaee-demo\\src\\com\\java\\training\\io\\stream\\Content";

		//read file into stream, try-with-resources
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			stream.forEach(System.out::println);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}