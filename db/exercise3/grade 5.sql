  select u.university_name
  ,s.id as roll_number
  ,s.name as student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name as college_name
  ,d.dept_name
  ,sr.semester
  ,sr.grade
  ,sr.credits
 from university u
  ,college c
  ,department d 
  ,designation de
  ,college_department cd
  ,employee e 
  ,student s
  ,syllabus sy
  ,professor_syllabus ps
  ,semester_result sr
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.college_id 
 and cd.udept_code = d.dept_code
 and e.college_id = c.college_id 
 and e.cdept_id = cd.cdept_id 
 and e.desig_id = de.id 
 and s.college_id = c.college_id
 and s.cdept_id = cd.cdept_id
 and sy.cdept_id = cd.cdept_id
 and ps.syllabus_id = sy.syl_id
 and sr.syllabus_id = sy.syl_id
 and sr.stud_id = s.id
 order by semester ;