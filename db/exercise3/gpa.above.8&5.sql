SELECT student.id
	  ,student.roll_number
      ,student.name AS STUDENT_NAME
      ,student.gender
      ,college.code
      ,college.name AS COLLEGE_NAME
      ,semester_result.grade
      ,semester_result.GPA
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id
   AND semester_result.stud_id = student.id
   AND semester_result.GPA > 8
ORDER BY semester_result.GPA;
 use `university`;  
SELECT student.id
	  ,student.roll_number
      ,student.name AS STUDENT_NAME
      ,student.gender
      ,college.code
      ,college.name AS COLLEGE_NAME
      ,semester_result.grade
      ,semester_result.GPA
  FROM university
      ,college
      ,student
      ,semester_result
 WHERE university.univ_code = college.univ_code
   AND student.college_id = college.college_id
   AND semester_result.stud_id = student.id
   AND semester_result.GPA > 5
ORDER BY semester_result.GPA;