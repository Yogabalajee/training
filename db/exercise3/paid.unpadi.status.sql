select de.name as designation_name  
  ,desg_rank
  ,u.univ_code
  ,c.name as college_name
  ,d.dept_name
  ,u.university_name
  ,c.city,c.state
  ,c.year_opened
 from university u
  ,college c
  ,department d 
  ,designation de
  ,college_department cd
  ,employee e 
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.college_id 
 and cd.udept_code = d.dept_code
 and e.college_id = c.college_id 
 and e.cdept_id = cd.cdept_id 
 and e.desig_id = de.id 
 order by desg_rank;