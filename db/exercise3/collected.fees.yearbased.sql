SELECT semester
      ,amount
      ,paid_status
      ,roll_number
      ,stud_name
      ,gender
      ,email
      ,phone
      ,college_name
      ,university_name
  FROM semester_fee semester_fee
  LEFT JOIN student student
    ON semester_fee.stud_id = student.id
  LEFT JOIN college college
    ON student.college_id = college.college_id
 INNER JOIN university university
    ON college.univ_code = university.univ_code
 WHERE semester_fee.paid_status = 'PAID'
   AND semester_fee.paid_year = '2019'
 ORDER BY student.id
 
 