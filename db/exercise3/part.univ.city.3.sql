select u.university_name
  ,s.id as roll_number
  ,s.name as student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name
  ,d.dept_name
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.college_id 
 and cd.udept_code = d.dept_code
 and s.college_id = c.college_id
 and s.cdept_id = cd.cdept_id
 and u.univ_code = '222'
 and c.city ='coimbatore';