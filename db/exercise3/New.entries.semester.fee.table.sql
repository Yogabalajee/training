 select u.university_name
  ,s.id as roll_number
  ,s.name as student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name as college_name
  ,d.dept_name
  ,sf.amount
  ,sf.paid_year
  ,sf.paid_status
 from university u
  ,college c
  ,department d 
  ,college_department cd
  ,student s
  ,syllabus sy
  ,semester_fee sf
 where c.univ_code = u.univ_code 
 and u.univ_code = d.univ_code 
 and cd.college_id = c.college_id 
 and cd.udept_code = d.dept_code
 and s.college_id = c.college_id
 and s.cdept_id = cd.cdept_id
 and sy.cdept_id = cd.cdept_id
 and sf.stud_id = s.id
 and sf.cdept_id = cd.cdept_id
 order by roll_number ;