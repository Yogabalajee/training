CREATE VIEW student_details
AS
SELECT stud_id
      ,stud_name
      ,college_id
  FROM student
 INNER JOIN 
      college
 USING (college_id);