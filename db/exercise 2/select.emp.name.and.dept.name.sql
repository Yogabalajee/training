SELECT employee_table.first_name
	  ,employee_table.surename
	  ,department_table.department
  FROM database.employee_table employee_table
       INNER JOIN department_table department_table
   ON (employee_table.dept_id = department_table.dept_id)