 CREATE TABLE `database`.`employee_table` (
  `emp_id` int NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `sure_name` varchar(45) DEFAULT NULL,
  `dob` date NOT NULL,
  `date_of_joining` date NOT NULL,
  `annual_salary` varchar(45) NOT NULL,
  `dept_id` int DEFAULT NULL);
  
  CREATE TABLE `database`.`department_table` (
  `dept_id` int NOT NULL,
  `department` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`));