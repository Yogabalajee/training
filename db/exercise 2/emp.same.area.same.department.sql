SELECT employee.first_name
      ,department.department 
	  ,employee.address
  FROM employee_table employee
      ,department_table department 
WHERE employee.dept_id=department.dept_id 
  AND department.department IN ('ITDesk')
  AND employee.address IN ('theni')
